<?php
    require_once 'bootstrap.php';

    if(isset($_POST["inputEmail"])){
        $login_result = $dbh->checkPresence($_POST["inputEmail"]);
        if(count($login_result)!=0){
            //Login fallito
            $templateParams["errorelogin"] = "Errore! Email già presente!";
        }
        else{
            $dbh->insertNewOrganizer($_POST["inputCitta"], $_POST["inputData"], $_POST["inputEmail"], $_POST["inputPasswordRegistrazione"], $_POST["inputNome"],
                                $_POST["inputCognome"], 3);
            header("Location: login.php");
            exit;
        }
    }

    //Base Template
    $templateParams["titolo"] = "PartYamo - Registrazione";
    //$templateParams["articoli"] =
    $templateParams["nome"] = 'register_page.php';
    $templateParams["js"] = array('js/validation.js');
    
    require 'template/base.php';
?>