<?php
require_once 'bootstrap.php';
 
//controlla se  è inserito
if(!isAdminLoggedIn()){
    header("location: admin_login.php");
}

if(isset($_POST["inputCategoria"])){
    $dbh->addCategory($_POST["inputCategoria"]);
} else if(isset($_GET["accetta"])) {
    $dbh->acceptOrganizerByUsername($_GET["accetta"]);
} else if(isset($_GET["elimina"])){
    $dbh->deleteUser($_GET["elimina"]);
}

//Base Template
$templateParams["titolo"] = "PartYamo - Area Riservata";
$templateParams["navOrganize"] = true;
$templateParams["nome"] = "admin_management.php";
$templateParams["admin"] = true;

require 'template/base.php';
?>