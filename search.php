<?php
if(isset($_GET["cerca"])){
    $templateParams["risultato"] = "Risultato della ricerca:";
    if($_GET["cerca"]==="uomini"){
        $risultato = $templateParams["evento"] = $dbh->eventsMoreMen();
    } else if($_GET["cerca"]==="donne"){
        $risultato =  $templateParams["evento"] = $dbh->eventsMoreWomen();
    } else {
        $risultato = $templateParams["evento"] = $dbh->searchEvents($_GET["cerca"]);
    }
    if(count($risultato)==0){
        $templateParams["erroreRicerca"] = "Nessun evento trovato";
    }
} else{
    $city = $dbh->getUserCity($_SESSION["username"])[0]["citta"];
    $risultato = $templateParams["evento"] = $dbh->getEventsByCity($city);
    if(count($risultato)==0){
        $templateParams["messaggio"] = "Peccato, non sono disponibili eventi nella tua città";
        $templateParams["evento"] = $dbh->getRandomEvents(6);
    }
}
?>