<?php
    require_once 'bootstrap.php';

    if(!isUserLoggedIn()){
        header("Location: login.php");
    }
    require 'search.php';

    //Base Template
    $templateParams["titolo"] = "PartYamo - Home";
    //$templateParams["articoli"] =
    $templateParams["nome"] = 'events_home.php';
    $templateParams["cerca"] = true;
    $templateParams["nav"] = true;
    $templateParams["home"] = true;
    $templateParams["js"] = array('js/navSlide.js');

    require 'template/base.php';
?>