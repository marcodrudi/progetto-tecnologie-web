<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

if(isset($_SESSION["username"])){
    if(isset($_GET["rm"])){
        $dbh->deleteEventInCart($_GET["rm"], $_SESSION["username"]);
    }

    $eventi = $dbh->getEventsOfUserCart($_SESSION["username"]);
    $templateParams["eventi"] = $eventi;
}


$templateParams["titolo"] = "PartYamo - Carrello";
$templateParams["nome"] = "eventi-carrello.php";
$templateParams["mode"] = "carrello";
$somma = $dbh->sumOfCartOfUserEvent($_SESSION["username"]);
$templateParams["totale"] = $somma[0];
$templateParams["nav"] = true;
$templateParams["home"] = true;
$templateParams["js"] = array('js/navSlide.js');

require 'template/base.php';
?>