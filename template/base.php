<!DOCTYPE html>
<html lang="it">
    <head>
        <title><?php echo $templateParams["titolo"]; ?></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="./css/style.css" />
    </head>
    
    <body>
        <div id="background"></div> <!-- Serve per l'immagine di sfondo-->

        <div id="page-container">
            <div id="content-wrap">

                <?php if(isset($templateParams["nav"])) { ?>
                <div class="upNav">
                    <nav class="navbar navbar-light" style="background-color: #B5596C;">
                        <form class="form-inline">
                            <button class="btn btn-secondary" type="button" id="sideMenuButton">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <a class="navbar-brand" style="color: white;">PARTYAMO</a>
                        </form>
                        <form class="form-inline">
                            <a href="carrello.php" onclick="setCookies()"><img src=<?php echo UPLOAD_DIR."carrello.png"?> title="Pulsante Carrello" alt="Carrello" width="40" height="40"/></a>
                            <?php if(isset($templateParams["cerca"])){?>
                                <button class="btn btn-secondary" type="button" id="lenteButton">
                                    <img src=<?php echo UPLOAD_DIR."lente.png"?> title="Pulsante Cerca" alt="Cerca" width="40" height="40"/></a>
                                </button>    
                            <?php } ?>
                        </form>
                    </nav>
                    
                    <!-- testo delle barre di navigazione-->
                    <div id="menuSidenav" class="sidenav">
                        <a href="javascript:void(0)" class="closebtn" id="closeSideNav">&times;</a>
                        <a href="user_home.php" onclick="setCookies()">Home</a>
                        <a href="notifiche.php" onclick="setCookies()">Notifiche</a>
                        <a href="eventi-acquistati.php" onclick="setCookies()">Eventi acquistati</a>
                        <div class="dropdown-divider"></div>
                        <a href="first_page.php">Logout</a>
                    </div>
                    
                    <form action="#" method="GET">
                        <div id="cercaSidenav" class="sidenav">
                            <a href="javascript:void(0)" class="closebtn" id="closeCercaNav">&times;</a>
                            <fieldset>
                                <h1 class="dropdown-header" style="color: #B5596C;">Categorie:</h1>
                                <?php $i = 1; foreach($dbh->getAllTypeOfEvent() as $categoria): ?>
                                <div class="form-check">
                                    <input class="form-check-input" name="cerca" type="radio" id="radioCerca<?php echo $i?>" value="<?php echo $categoria["nome_Tipologia_Evento"]?>">
                                    <label class="form-check-label" for="radioCerca<?php echo $i?>">
                                        <?php echo $categoria["nome_Tipologia_Evento"]?>
                                    </label>
                                </div>
                                <?php $i=$i+1; endforeach; ?> 
                                <h1 class="dropdown-header" style="color: #B5596C;">Città:</h1>
                                <?php foreach($dbh->getCities() as $citta): ?>
                                <div class="form-check">
                                    <input class="form-check-input" name="cerca" type="radio" id="radioCerca<?php echo $i?>" value="<?php echo $citta["citta"]?>">
                                    <label class="form-check-label" for="radioCerca<?php echo $i?>">
                                        <?php echo $citta["citta"]?>
                                    </label>
                                </div>
                                <?php $i=$i+1; endforeach; ?>
                                <h1 class="dropdown-header" style="color: #B5596C;">Opzioni:</h1>
                                <div class="form-check">
                                    <input class="form-check-input" name="cerca" type="radio" id="radioCerca<?php echo $i?>" value="uomini">
                                    <label class="form-check-label" for="radioCerca<?php echo $i?>">
                                        Più uomini che donne
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" name="cerca" type="radio" id="radioCerca<?php echo $i+1?>" value="donne">
                                    <label class="form-check-label" for="radioCerca<?php echo $i+1?>">
                                        Più donne che uomini
                                    </label>
                                </div>
                            </fieldset>
                            <div style="text-align: right;">
                                <button onclick="setCookies()" class="btn btn-outline-light" id="submitCercaButton" type="submit">Cerca</button>
                            </div>
                        </div>
                    </form>
                <?php } ?>

                <!-- barra in alto organizzatore-->
                <?php if(isset($templateParams["navOrganize"])) { ?>     
                    <div class="upNav">
                        <nav class="navbar navbar-light" style="background-color: #B5596C;">
                            <form class="form-inline">                                                                                
                                <a class="navbar-brand" style="color: white;">PARTYAMO</a>
                            </form>
        
                            <form class="form-inline">
                                <?php if(isset($templateParams["notOrg"])): ?>
                                <a onclick="setCookies()" class="navbar-brand" id="viewNotify"  href="notifiche.php">Notifiche</a>
                                <?php endif; ?>
                                <a class="navbar-brand" id="logoutOrganized" href="first_page.php">Logout</a>
                                </form>
                        </nav>
                    </div>   
                <?php } ?>
                

                <main>
                    <?php
                        if(isset($templateParams["nome"])){
                            require($templateParams["nome"]);
                        }
                    ?>
                </main>
            </div>

            <footer id="main-footer">
                <p>© 2019 Copyright: - PartYamo - Via aaaaaa, 122 - Partita IVA: 011317103 - Tutti i diritti sono riservati</p>
                <?php if(!isset($templateParams["admin"])): ?>
                <a href="admin_login.php">Area Riservata</a>
                <?php endif; ?>
            </footer>
        </div>
        
        <!--cookies banner-->
        <?php if(isset($_SESSION["username"])) {
            if(!isset($_COOKIE["username"]) || $_COOKIE["username"] != $_SESSION["username"]){ ?> <!-- se i cookie non sono stati accettati allora li visualizzo-->
            <div class="banner">
                <p> Utilizziamo i cookies per garantire la funzionalità del sito. <br/>
                    Continuando a navigare sul sito o cliccando sul pulsante "Accetto", l'utente accetta di utilizzare i cookies.
                    <button class="btn btn-outline-light" onclick="setCookies()"> Accetto </button>
                </p> 
            </div>
        <?php }
        } ?>
        
        <script>
            function setCookies() {
                <?php if(isset($_SESSION["username"])) {
                    if(!isset($_COOKIE["username"]) || $_COOKIE["username"] != $_SESSION["username"]){ ?>
                        $username = '<?php echo $_SESSION["username"]?>';
                        document.cookie = "username="+$username;
                <?php } 
                } ?>
            }
        </script>

        <!-- servono per gli elementi dropdown -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>
        <script src="js/setCookies.js"></script>
        <?php
            if(isset($templateParams["js"])):
                foreach($templateParams["js"] as $script):
            ?>
                <script src="<?php echo $script; ?>"></script>
            <?php
                endforeach;
            endif;
        ?>
    </body>
</html>