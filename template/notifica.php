                        
                        <?php if(!isset($templateParams["notifiche"])): ?>
                            <div class="alert alert-light" role="alert">
                                Non è presente nessuna notifica.
                            </div>
                        <?php else: ?>
                            <?php foreach ($templateParams["notifiche"] as $notifica) : ?>
                                <article class="anteprima-articolo">
                                    <img class="previewImage" src="<?php echo UPLOAD_DIR.$notifica["immagine"]; ?>" class="img-fluid" alt="category image">
                                    <div class="article-text">
                                        <p class="categoria"><?php echo $notifica["nome_Tipologia_Evento"]; ?></p>
                                        <h1><?php echo $notifica["descrizione"]; ?></h1>
                                        <p class="descrizione"><?php echo $notifica["nome"];  ?> - <?php echo $notifica["data"]; ?></p>
                                        <p class="descrizione">Prezzo: <?php echo $notifica["prezzo"]; ?>€</p>
                                        <footer class="article-footer">
                                            <a class="btn btn-outline-light" type="button" href="evento.php?evento=<?php echo $notifica["id_Evento"];?>">Leggi tutto</a>
                                        </footer>
                                    </div>
                                </article>
                            <?php endforeach; ?>
                        <?php endif; ?>