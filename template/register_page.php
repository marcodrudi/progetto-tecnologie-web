                    <form class="form-signin register-validation" action="#" method="POST">
                        <div class="text-center mb-4">
                            <h1 class="h3 mb-3">Registrazione</h1>
                        </div>
                        <?php if(isset($templateParams["errorelogin"])): ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo $templateParams["errorelogin"]; ?>
                            </div>
                        <?php endif; ?>
                        <div class="form-label-group">
                            <input type="text" id="inputNome" name="inputNome" class="form-control" placeholder="Nome">
                            <label class="descrizione" for="inputNome">Nome</label>
                        </div>
                        <div class="form-label-group">
                            <input type="text" id="inputCognome" name="inputCognome" class="form-control" placeholder="Congnome">
                            <label class="descrizione" for="inputCognome">Cognome</label>
                        </div>
                        <div class="form-label-group">
                            <input type="text" id="inputCitta" name="inputCitta" class="form-control" placeholder="Città di residenza">
                            <label class="descrizione" for="inputCitta">Città di residenza</label>
                        </div>
                        <div class="form-label-group">
                            <input type="date" id="inputData" name="inputData" class="form-control">
                            <label class="descrizione" for="inputData">Data di nascita</label>
                        </div>
                        <?php if(isset($templateParams["user"])): ?>
                        <div class="form-label-group">
                            <select class="form-control" id="sessoSelezionato" name="sessoSelezionato">
                                <option value="M">Maschio</option>
                                <option value="F">Femmina</option>
                            </select>
                        </div>
                        <?php endif; ?>
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" name="inputEmail" class="form-control" placeholder="Email address">
                            <label class="descrizione" for="inputEmail">Email</label> 
                            <small class="form-text text-muted">
                                La tua email sarà il tuo username
                            </small>
                        </div>
                        <div class="form-label-group">
                            <input type="password" id="inputPasswordRegistrazione" name="inputPasswordRegistrazione" class="form-control" placeholder="Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}">
                            <label class="descrizione" for="inputPasswordRegistrazione">Password</label>
                            <small class="form-text text-muted">
                                La tua password deve essere lunga dagli 8 ai 20 caratteri, deve contenere una lettera minuscola, una maiuscola e un numero.
                            </small>
                        </div>
                        <div class="form-label-group">
                            <input type="password" id="confermaPassword" name="confermaPassword" class="form-control" placeholder="ripetiPassword">
                            <label class="descrizione" for="confermaPassword">Conferma password</label>
                        </div>
                        <div style="text-align: right;">
                            <button class="btn btn-outline-light" id="submitButton" type="submit">Registrati</button>
                        </div>
                    </form>