                <?php if($templateParams["notifica"]==2): ?>
                    <div class="alert alert-light" role="alert">
                        Aggiunto in lista partecipanti!
                    </div>
                <?php else: ?>
                    <?php if($templateParams["notifica"]==3): ?>
                        <div class="alert alert-light" role="alert">
                            Rimosso dalla lista partecipanti!
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                <?php $evento = $templateParams["evento"] ?>
                <article id="anteprima-articolo">
                    <div class="containerImageComplete" style="background:url('<?php echo UPLOAD_DIR.$evento["immagine"]; ?>') center top / cover no-repeat;>";></div>
                    <div class="article-text">
                    <p class="categoria categoriaTotale"><?php echo $evento["nome_Tipologia_Evento"]; ?></p>
                    <h1 class="titoloTotale"><?php echo $evento["nome"]; ?></h1>
                    <p class="dettagliTotali"><?php setLocale(LC_TIME, "Italian"); echo strftime("%e %B %Y", strtotime($evento["data"]))?><br/>
                        A partire dalle <?php echo strftime("%H:%M", strtotime($evento["orario"])) ?></p>
                    <p class="p-3 mb-2 bg-secondary text-white descrizioneTotale"><?php echo $evento["descrizione"]; ?></p>
                    <p class="dettagliTotali">Presso <?php echo $evento["luogo"]; ?> - <?php echo $evento["citta"]; ?> - <?php echo $evento["indirizzo"]; ?> n <?php echo $evento["numero_civico"]; ?></p>
                    <p class="dettagliTotali">Prezzo: <?php echo $evento["prezzo"]; ?>€</p>
                    <p class="text-primary dettagliTotali">Numero partecipanti uomini: <?php echo $evento["numero_partecipanti_uomini"]; ?></p>
                    <p class="text-danger dettagliTotali">Numero partecipanti donne: <?php echo $evento["numero_partecipanti_donne"]; ?></p>
                    <?php if(isset($templateParams["user"])): ?>
                    <footer class="article-footer">
                        <?php if($dbh->isPresentEventInCartOfUsers($evento["id_Evento"], $_SESSION["username"])): ?>
                            <p class="aggiungi">Evento già presente nel carrello</p>
                        <?php else: ?>
                            <?php if($dbh->isSoldOutEvent($evento["id_Evento"])): ?>
                                <?php if($evento["prezzo"] == 0): ?>
                                    <?php if($dbh->isPresentEventInUserShop($evento["id_Evento"], $_SESSION["username"])): ?>
                                        <a class="btn btn-outline-light aggiungi" role="button" href="acquista.php?action=3&evento=<?php echo $evento["id_Evento"];?>">Rimuovi partecipazione</a>
                                    <?php else: ?>
                                        <p class="aggiungi">Evento sold out!</p>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <p class="aggiungi">Evento sold out!</p>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php if($evento["prezzo"] == 0): ?>
                                    <?php if($dbh->isPresentEventInUserShop($evento["id_Evento"], $_SESSION["username"])): ?>
                                        <a class="btn btn-outline-light aggiungi" role="button" href="acquista.php?action=3&evento=<?php echo $evento["id_Evento"];?>">Rimuovi partecipazione</a>
                                    <?php else: ?>
                                        <a class="btn btn-outline-light aggiungi" role="button" href="acquista.php?action=2&evento=<?php echo $evento["id_Evento"];?>">Partecipa</a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a class="btn btn-outline-light aggiungi" role="button" href="evento.php?evento=<?php echo $evento["id_Evento"];?>&add=<?php echo $evento["id_Evento"];?>">Aggiungi al carrello</a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>    
                    </footer>
                    <?php endif; ?>
                    </div>
                </article>