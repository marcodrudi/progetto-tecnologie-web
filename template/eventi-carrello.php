                <?php if($templateParams["mode"] == "eventi-acquistati"): ?>
                    <div class="alert alert-light" role="alert">
                        Eventi acquistati
                    </div>
                <?php endif; ?>

                <?php if($templateParams["mode"] == "carrello"): ?>
                    <?php if(!isset($templateParams["eventi"])): ?>
                        <div class="alert alert-light" role="alert">
                            Non è presente nessun evento nel carrello.
                        </div>
                    <?php else: ?>
                        <?php $tet = $templateParams["totale"]; ?>
                        <div class="alert alert-light" role="alert">
                            <p>Carrello - Totale: <?php echo $tet["somme"]; ?>€</p>
                            <hr>
                            <a class="btn btn-outline-dark" role="button" href="acquista.php" id="acquista">Procedi all'acquisto</a>
                        </div>
                        <?php foreach ($templateParams["eventi"] as $evento) : ?>
                            <article class="anteprima-articolo">
                                <img class="previewImage" src="<?php echo UPLOAD_DIR.$evento["immagine"]; ?>" class="img-fluid" alt="category image">
                                <div class="article-text">
                                    <p class="categoria"><?php echo $evento["nome_Tipologia_Evento"]; ?></p>
                                    <h1 ><?php echo $evento["nome"];  ?> - <?php setLocale(LC_TIME, "Italian"); echo strftime("%e %B", strtotime($evento["data"]))?></h1>
                                    <p class="descrizione"><?php echo $evento["breve_descrizione"]; ?></p>
                                    <h2>Prezzo: <?php echo $evento["prezzo"]; ?>€</h2>
                                    <footer class="article-footer">
                                        <a class="btn btn-outline-light" role="button" href="evento.php?evento=<?php echo $evento["id_Evento"];?>">Leggi tutto</a>
                                        <a class="btn btn-outline-light" role="button" href="carrello.php?rm=<?php echo $evento["id_Evento"];?>">Rimuovi dal carrello</a>
                                    </footer>
                                </div>
                            </article>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if(!isset($templateParams["eventi"])): ?>
                        <div class="alert alert-light" role="alert">
                            Non è presente nessun evento.
                        </div>
                    <?php else: ?>
                        <?php foreach ($templateParams["eventi"] as $evento) : ?>
                            <article class="anteprima-articolo">
                                <img class="previewImage" src="<?php echo UPLOAD_DIR.$evento["immagine"]; ?>" class="img-fluid" alt="category image">
                                <div class="article-text">
                                    <p class="categoria"><?php echo $evento["nome_Tipologia_Evento"]; ?></p>
                                    <h1><?php echo $evento["nome"];  ?> - <?php setLocale(LC_TIME, "Italian"); echo strftime("%e %B", strtotime($evento["data"]))?></h1>
                                    <p class="descrizione"><?php echo $evento["breve_descrizione"]; ?></p>
                                    <footer class="article-footer">
                                    <?php if(isset($templateParams["home"])): ?>
                                        <a class="btn btn-outline-light" role="button" href="evento.php?evento=<?php echo $evento["id_Evento"];?>">Leggi tutto</a>
                                    <?php endif; ?>
                                    </footer>
                                </div>
                            </article>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endif; ?>
                
                
