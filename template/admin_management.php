                    <h1 class="admin">Area Riservata</h1>
                    <p>
                        <h2 class="admin">Aggiungi Categoria</h2>
                        <section>
                            <h3 class="admin">Tutte le categorie:</h3>
                            <p class="admin">
                            <?php $i = 1; foreach($dbh->getAllTypeOfEvent() as $categoria): ?>
                            - <?php echo $categoria["nome_Tipologia_Evento"]?></br>       
                            <?php $i=$i+1; endforeach; ?> 
                            </p>
                        </section>
                        <form class="admin" action="#" method="POST">
                            <div class="form-row align-items-left">
                                <div class="col-auto">
                                    <label class="descrizione" for="inputCategoria">Nuova categoria:</label>
                                    <input type="text" id="inputCategoria" name="inputCategoria" class="form-control mb-2" placeholder="Categoria"/>
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-outline-light mb-2" id="creaCategoria" type="submit">Inserisci</button>
                                </div>
                            </div>
                        </form>
                    </p>
                    <p>
                        <h2 class="admin">Accetta Organizzatore</h2>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-striped table-dark  mb-0">
                                <thead>
                                <tr style="background-color:#B5596C;">
                                    <th scope="col" id="numberAccetta">#</th>
                                    <th scope="col" id="nomeAccetta">Nome</th>
                                    <th scope="col" id="cognomeAccetta">Cognome</th>
                                    <th scope="col" id="usernameAccetta">Username</th>
                                    <th scope="col" id="gestioneAccetta">Gestisci</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($dbh->getAllNotAcceptedOrganizers() as $organizer) : ?>
                                <tr>
                                    <th scope="row" id="number<?php echo $i ?>Accetta"><?php echo $i ?></th>
                                    <td headers="number<?php echo $i ?>Accetta nomeAccetta"><?php echo $organizer["nome"]; ?></td>
                                    <td headers="number<?php echo $i ?>Accetta cognomeAccetta"><?php echo $organizer["cognome"]; ?></td>
                                    <td headers="number<?php echo $i ?>Accetta usernameAccetta"><?php echo $organizer["username"]; ?></td>
                                    <td class="gestisci" header="number<?php echo $i ?>Accetta gestione"><a href="admin.php?accetta=<?php echo $organizer["username"]; ?>">Accetta</a></td>
                                </tr>
                                <?php $i++; endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </p>
                    <p>
                        <h2 class="admin">Rimuovi Utente</h2>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-striped table-dark  mb-0">
                                <thead>
                                <tr style="background-color:#B5596C;">
                                    <th scope="col" id="numberElimina">#</th>
                                    <th scope="col" id="nomeElimina">Nome</th>
                                    <th scope="col" id="cognomeElimina">Cognome</th>
                                    <th scope="col" id="usernameElimina">Username</th> 
                                    <th scope="col" id="tipologiaElimina">Tipologia</th>
                                    <th scope="col" id="gestioneElimina">Gestisci</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($dbh->getAllUsers() as $user) : ?>
                                <tr>
                                    <th scope="row" id="number<?php echo $i ?>Elimina"><?php echo $i ?></th>
                                    <td headers="number<?php echo $i ?>Elimina nomeElimina"><?php echo $user["nome"]; ?></td>
                                    <td headers="number<?php echo $i ?>Elimina cognomeElimina"><?php echo $user["cognome"]; ?></td>
                                    <td headers="number<?php echo $i ?>Elimina usernameElimina"><?php echo $user["username"]; ?></td>
                                    <td headers="number<?php echo $i ?>Elimina tipologiaElimina">Utente</td>
                                    <td class="gestisci" header="number<?php echo $i ?>Elimina gestioneElimina"><a href="admin.php?elimina=<?php echo $user["username"]; ?>">Elimina</a></td>
                                </tr>
                                <?php $i++; endforeach; ?>
                                <?php foreach($dbh->getAllOrganizers() as $organizer) : ?>
                                <tr>
                                <th scope="row" id="number<?php echo $i ?>Elimina"><?php echo $i ?></th>
                                    <td headers="number<?php echo $i ?>Elimina nomeElimina"><?php echo $organizer["nome"]; ?></td>
                                    <td headers="number<?php echo $i ?>Elimina cognomeElimina"><?php echo $organizer["cognome"]; ?></td>
                                    <td headers="number<?php echo $i ?>Elimina usernameElimina"><?php echo $organizer["username"]; ?></td>
                                    <td headers="number<?php echo $i ?>Elimina tipologiaElimina">Organizzatore</td>
                                    <td class="gestisci" header="number<?php echo $i ?>Elimina gestioneElimina"><a href="admin.php?elimina=<?php echo $user["username"]; ?>">Elimina</a></td>
                                </tr>
                                <?php $i++; endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </p>