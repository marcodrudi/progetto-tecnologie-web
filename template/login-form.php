                    <form class="form-signin login-validation" action="#" method="POST">
                        <div class="text-center mb-4">
                            <img class="mb-4" src="<?php echo UPLOAD_DIR."login-user.png"; ?>" alt="">
                            <h1 class="h3 mb-3"><?php echo $templateParams["descrizione"]; ?></h1>
                        </div>
                        <?php if(isset($templateParams["errorelogin"])): ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo $templateParams["errorelogin"]; ?>
                            </div>
                        <?php endif; ?>
                        <div class="form-label-group">
                            <input type="email" id="inputUsername" name="inputUsername" class="form-control" placeholder="Email address" required autofocus>
                            <label class="descrizione" for="inputUsername">Username</label>
                        </div>
                        <div class="form-label-group">
                            <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required>
                            <label class="descrizione" for="inputPassword">Password</label>
                        </div>
                        <div style="text-align: right;">
                            <button class="btn btn-outline-light" type="submit">Accedi</button>
                        </div>
                        <?php if(isset($templateParams["user"])): ?>
                        <p>
                            Non sei ancora iscritto? <a href="user_register.php">Registrati</a><br/>
                            Hai eventi da proporre? <a href="organizer_register.php">Registrati</a>
                        </p>
                        <?php endif; ?>
                    </form>