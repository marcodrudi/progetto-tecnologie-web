                <?php foreach($templateParams["evento"] as $evento) : ?>
                    
                    <article>
                        <img class="previewImage" src="<?php echo UPLOAD_DIR.$evento["immagine"]; ?>" class="img-fluid" alt="category image">
                        <div class="article-text">
                            <p class="categoria"><?php echo $evento["categoria"]?></p>
                            <h1> <?php echo $evento["nome"]?> - <?php setLocale(LC_TIME, "Italian"); echo strftime("%e %B", strtotime($evento["data"]))?></h1>
                            <p class="descrizione"><?php echo $evento["breve_descrizione"]?></p>
                            <p class="luogo"><?php echo $evento["luogo"]?> - <?php echo $evento["citta"]?></p>
                            <?php if(isset($templateParams["home"])){ ?>
                                <footer class="article-footer">
                                <a class="btn btn-outline-light" role="button" href="evento.php?evento=<?php echo $evento["id_Evento"];?>">Leggi tutto</a>
                                </footer>
                            <?php } ?>

                            <?php if(isset($templateParams["navOrganize"])){ ?>
                                <footer class="article-footer">
                                    <a class="btn btn-outline-light" id="modifyEvents" role="button" href="manage_events.php?action=2&id=<?php echo $evento["id_Evento"]; ?>">Modifica</a>
                                    <a class="btn btn-outline-light" id="deleteEvents" role="button" href="organizer_home.php?action=3&id=<?php echo $evento["id_Evento"]; ?>">Elimina</a>
                                </footer>
                            <?php }?>
                        </div>
                    </article>
                <?php endforeach; ?>
