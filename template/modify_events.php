<?php 
    $evento = $templateParams["evento"]; 
    $azione = getAction($_GET["action"]);
    //var_dump($templateParams["evento"]);

    if($_GET["action"] == 2){
        $act = "action=2&id=".$_GET['id'];
    }else{
        $act = "action=1";
    }
?>

<form class="form-signin modify-validation" id="modifyEvents" action="organizer_home.php?<?php echo $act ?>" method="POST" enctype="multipart/form-data">
       
        
            
                <div class="form-label-group">
                    <label id=nomeEvento for="nome">Nome evento:</label
                    ><input type="text" id="nome" name="nome" value="<?php echo $evento["nome"]; ?>">
                </div>

                <div class="form-label-group">
                    <label for="descrizione">Descrizione evento:</label>
                    
                    <textarea id="descrizione" name="descrizione"><?php echo $evento["descrizione"]; ?></textarea>
                </div>

                <div class="form-label-group">
                    <label for="breve_descrizione">Breve descrizione evento:</label>
                    <textarea id="breve_descrizione" name="breve_descrizione"><?php echo $evento["breve_descrizione"]; ?></textarea>
                </div>

                <div class="form-label-group">
                    <label  for="tipologia_eventi">Tipologia eventi:</label>
                    <div class="form-label-group">
                        <select class="form-control" id="tipologia_eventi" name="tipologia_eventi">
                        <?php foreach($dbh->getAllTypeOfEvent() as $typeEventi): ?>                
                                <option value="<?php echo $typeEventi["nome_Tipologia_Evento"]; ?>"><?php echo $typeEventi["nome_Tipologia_Evento"]; ?></option>                                         
                        <?php endforeach; ?>
                        </select> 
                    </div>
                </div>

                <div class="form-label-group">
                            <label id="orario_inizio" for="orario">Orario inizio</label>
                            <input type="time" id="orario" name="orario" class="form-control" placeholder="Orario inizio evento" value="<?php echo $evento["orario"]?>">
                </div> 

                 <div>
                        <div class="form-label-group">
                            <label id="data" for="data">Data Evento</label>
                            <input type="date" id="data" name="data" class="form-control" placeholder="Data Evento" value="<?php echo $evento["data"]?>">                           
                        </div>
                 </div>
                
                <div class="form-label-group">
                    <?php if($_GET["action"]==2): ?>
                    <label for="immagine">Immagine Evento</label><input type="file" name="immagine1" id="immagine" />
                    <img src="<?php echo UPLOAD_DIR.$evento["immagine"]; ?>" alt="" />
                    <?php endif; ?>
                    <?php if($_GET["action"]==1): ?>
                    <label for="immagine">Immagine Evento</label><input type="file" name="immagine" id="immagine" />
                    <?php endif; ?>
                </div>

                <div class="form-label-group">
                    <label for="prezzo">Costo evento:</label>
                    <input type="text" id="prezzo" name="prezzo" value="<?php echo $evento["prezzo"]; ?>" />
                </div>

                <div class="form-label-group">
                    <label for="numero_Partecipanti_Max">Numero Partecipanti Max:</label>
                    <input type="text" id="numero_partecipanti_Max" name="numero_partecipanti_Max" value="<?php echo $evento["numero_partecipanti_Max"]; ?>" />
                </div>

                <div class="form-label-group">
                    <label for="luogo">Nome locale:</label>
                    <input type="text" id="luogo" name="luogo" value="<?php echo $evento["luogo"]; ?>" />
                </div>

                <div class="form-label-group">
                    <label for="indirizzo">Indirizzo evento:</label>
                    <input type="text" id="indirizzo" name="indirizzo" value="<?php echo $evento["indirizzo"]; ?>" />
                </div>
                
                <div class="form-label-group">
                    <label for="numero_civico">Numero civico:</label><input type="text" id="numero_civico" name="numero_civico" value="<?php echo $evento["numero_civico"]; ?>" />
                </div>

                <div class="form-label-group">
                    <label for="citta">Città:</label>
                    <input type="text" id="citta" name="citta" value="<?php echo $evento["citta"]; ?>" />
                </div>
                
                <?php if($_GET["action"]!=1): ?>
                <div>
                    <input type="hidden" name="oldimg" value="<?php echo $evento["immagine"]; ?>" />
                </div>
                <?php endif; ?>

                <div id="azione">
                    <!--<input type="submit" name="submit" value="<?//php echo $azione; ?> Evento" />-->
                    <button class="btn btn-outline-light" type="submit" onclick="setCookies()"><?php echo $azione; ?> Evento</button>
                    <a href="organizer_home.php" id="annulla">Annulla</a>
                </div>


            
        
</form>