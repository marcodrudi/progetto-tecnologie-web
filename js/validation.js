$(document).ready(function() {

  jQuery.validator.addMethod("pattern", function(value, element) {
    if(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$/.test(value)){
      return true;
    }
    else{
      return false;
    }
  }, "Inserire una password valida");

  $(".register-validation").validate({
    rules: {
      inputNome: "required",
      inputCognome: "required",
      inputCitta: "required",
      inputData: "required",
      sessoSelezionato: "required",
      inputEmail: {
        required: true,
        minlength: 2
      },
      inputPasswordRegistrazione: {
        required: true,
        minlength: 8,
        maxlength: 20,
        pattern: true
      },
      confermaPassword: {
        required: true,
        equalTo: "#inputPasswordRegistrazione"
      },
      
    },
    messages: {
      inputNome: "INSERIRE IL NOME",
      inputCognome: "INSERIRE IL COGNOME",
      inputCitta: "INSERIRE LA CITTA'",
      inputData: "INSERIRE LA DATA",
      sessoSelezionato: "INSERIRE IL SESSO",
      inputEmail: {
        required: "INSERIRE LA EMAIL",
        minlength: "EMAIL NON VALIDA"
      },
      inputPasswordRegistrazione: {
        required: "INSERIRE PASSWORD",
        minlength: "PASSWORD TROPPO CORTA",
        maxlength: "PASSWORD TROPPO LUNGA",
        pattern: "PASSWORD NON CORRETTA"
      },
      confermaPassword: {
        required: "",
        equalTo: "LE PASSWORD NON SONO UGUALI"
      },  
    },
    submitHandler: function(form) {
      $(form).Submit();
    }
  });

  $(".login-validation").validate({
    onkeyup: false,
    rules: {
      inputUsername: {
        onkeyup: false,
        required: true,
        minlength: 2
      },
      inputPassword: {
        required: true,
      },      
    },
    messages: {
      inputUsername: {
        email: "INSERIRE USERNAME VALIDO",
        required: "INSERIRE USERNAME",
        minlength: "USERNAME NON VALIDO"
      },
      inputPassword: {
        required: "INSERIRE PASSWORD",
      }, 
    },
    submitHandler: function(form) {
      $(form).Submit();
    }
  });

  $(".modify-validation").validate({
    rules: {
      nome: "required",
      descrizione: "required",
      breve_descrizione: "required",
      tipologia_evento_selezionato: "required",
      data: "required",
      immagine: "required",
      orario: "required",
      prezzo: {
        required: true,
        number: true
      },
      numero_partecipanti_Max: {
        required: true,
        number: true
      },
      luogo: "required",
      indirizzo: "required",
      numero_civico: {
        required: true,
        number: true
      },
      citta: "required",  
    },
    messages: {
      nome:  "INSERIRE IL NOME DELL'EVENTO",
      descrizione: "INSERIRE DESCRIZIONE",
      breve_descrizione: "INSERIRE BREVE DESCRIZIONE",
      tipologia_evento_selezionato: "SCEGLIERE CATEGORIA",
      data: "SCEGLIERE DATA",
      immagine: "INSERIRE IMMAGINE",
      orario: "INSERIRE ORARIO",
      prezzo: {
        required: "INSERIRE PREZZO",
        number: "IL PREZZO DEVE ESSERE UN NUMERO"
      },
      numero_partecipanti_Max: {
        required: "INSERIRE NUMERO MASSIMO DI PARTECIPANTI",
        number: "IL NUMERO MASSIMO DI PARTECIPANTI DEVE ESSERE UN NUMERO"
      },
      luogo: "INSERIRE LUOGO",
      indirizzo: "INSERIRE INDIRIZZO",
      numero_civico: {
        required: "INSERIRE NUMERO CIVICO",
        number: "IL NUMERO CIVICO DEVE ESSERE UN NUMERO"
      },
      citta: "INSERIRE CITTA'",
       
    },
    submitHandler: function(form) {
      $(form).Submit();
    }
  });
});