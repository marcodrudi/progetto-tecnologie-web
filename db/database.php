<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }  

    //mi ritorna tutti gli user name gia inseriti
    public function checkPresence($username){
        $query = "SELECT username as Utente FROM user WHERE username= ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertNewUser($città,$dataNascita,$username,$pass,$name,$surname,$sex,$id_TipologiaUser){
        // Crea una chiave casuale
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        // Crea una password usando la chiave appena creata.
        $password = hash('sha512', $pass.$random_salt);
        $query="INSERT INTO `user`(`accettazione`, `citta`, `data_Nascita`, `username`, `pass`, `salt`,  `nome`, `cognome`, `sesso`, `id_TipologiaUser`) VALUES ('t',?,?,?,?,?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $city=strtolower($città);
        $stmt->bind_param('ssssssssi',$city,$dataNascita,$username,$password,$random_salt,$name,$surname,$sex,$id_TipologiaUser);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;     
    }

    public function insertNewOrganizer($città,$dataNascita,$username,$pass,$name,$surname,$id_TipologiaUser){
        // Crea una chiave casuale
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        // Crea una password usando la chiave appena creata.
        $password = hash('sha512', $pass.$random_salt);
        $query="INSERT INTO `user`(`accettazione`, `citta`, `data_Nascita`, `username`, `pass`, `salt`, `nome`, `cognome`,`id_TipologiaUser`) VALUES ('f',?,?,?,?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $city=strtolower($città);
        $stmt->bind_param('sssssssi',$city,$dataNascita,$username,$password,$random_salt,$name,$surname,$id_TipologiaUser);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;     
    }

    public function insertEvent($prezzo, $città, $descrizione, $breveDescrizione, $data, $numeroPartecipanti, $nome, $luogo, $indirizzo, $numeroCivico, $numeroUomini, $numeroDonne, $immagine, $orario, $idTipologiaEvento, $creatore){
        $query = "INSERT INTO EVENTO (prezzo, citta, descrizione, breve_descrizione, data, numero_partecipanti_Max, nome, luogo, indirizzo, numero_civico, numero_partecipanti_uomini, numero_partecipanti_donne, immagine, orario, id_Tipologia_Evento, creatore) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $city=strtolower($città);
        $stmt->bind_param('issssisssiiissis',$prezzo, $city, $descrizione, $breveDescrizione, $data, $numeroPartecipanti, $nome, $luogo, $indirizzo, $numeroCivico, $numeroUomini, $numeroDonne, $immagine, $orario, $idTipologiaEvento, $creatore);
        $stmt->execute();
        return $stmt->get_result();
    }

    public function getRandomEvents($n=2){
        $stmt = $this->db->prepare("SELECT evento.id_Evento, evento.nome, evento.breve_descrizione, tipologia_evento.nome_Tipologia_Evento as categoria, data, citta, immagine, luogo FROM evento, tipologia_evento WHERE evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function searchEvents($option){
        $stmt = $this->db->prepare("SELECT evento.id_Evento, nome, breve_descrizione, tipologia_evento.nome_Tipologia_Evento as categoria, data, citta, immagine, luogo FROM evento, tipologia_evento WHERE evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento AND (nome_Tipologia_Evento = ? OR citta = ?)");
        $stmt->bind_param('ss',$option, $option);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByCity($city){
        $stmt = $this->db->prepare("SELECT evento.id_Evento, nome, breve_descrizione, tipologia_evento.nome_Tipologia_Evento as categoria, data, citta, immagine, luogo FROM evento, tipologia_evento WHERE evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento AND citta = ?");
        $stmt->bind_param('s',$city);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getUserCity($username){
        $stmt = $this->db->prepare("SELECT citta FROM user WHERE username = ?");
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventById($id){
        $stmt = $this->db->prepare("SELECT id_Evento, prezzo, citta, descrizione, breve_descrizione, data, numero_partecipanti_Max, nome, luogo, indirizzo, numero_civico, numero_partecipanti_uomini, numero_partecipanti_donne, immagine, orario, tipologia_evento.nome_Tipologia_Evento, creatore, luogo FROM evento, tipologia_evento WHERE evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento AND id_Evento = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteEventById($id){

        $this->deleteFromNotify($id);
        $this->deleteFromCart($id);
        $this->deleteFromAcquisti($id);        
        
        $stmt = $this->db->prepare("DELETE FROM EVENTO WHERE id_Evento = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        return $stmt->get_result();
    }
    public function deleteFromCart($id){
        $stmt = $this->db->prepare("DELETE FROM carrello WHERE id_Evento = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        return $stmt->get_result();
    }
    public function deleteFromAcquisti($id){
        $stmt = $this->db->prepare("DELETE FROM acquisti WHERE id_Evento = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        return $stmt->get_result();
    }
    public function deleteFromNotify($id){
        $stmt = $this->db->prepare("DELETE FROM notifica WHERE id_Evento = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        return $stmt->get_result();
    }


    public function updateEvent($idEvento, $prezzo, $città, $descrizione, $breveDescrizione, $data, $numeroPartecipanti, $nome, $luogo, $indirizzo, $numeroCivico, $immagine, $orario, $id_TipologiaEvento){
        
        $query = "UPDATE `evento` SET `prezzo`=?,`citta`=?, `descrizione`=?,`breve_descrizione`=?,`data`=?,`numero_partecipanti_Max`=?,`nome`=?,`luogo`=?,`indirizzo`=?,`numero_civico`=?,`immagine`=?, `orario`=?, `id_Tipologia_Evento`=? WHERE `id_Evento`=?";
        $stmt = $this->db->prepare($query);
        $city=strtolower($città);
        $stmt->bind_param('issssisssissii', $prezzo, $city, $descrizione, $breveDescrizione, $data, $numeroPartecipanti, $nome, $luogo, $indirizzo, $numeroCivico, $immagine, $orario, $id_TipologiaEvento, $idEvento);
        $stmt->execute();

        $this->addNotify(2,1,$idEvento);
        return $stmt->get_result();
    }

    public function acceptOrganizerByUsername($username){
        $query = "UPDATE USER SET accettazione = 't' WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        return $stmt->execute();
    }

    public function getCategoriesById($id){
        $stmt = $this->db->prepare("SELECT nome_Tipologia_Evento FROM TIPOLOGIA_EVENTO WHERE id_Tipologia_Evento = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsOfUserCart($username){
        $query = "SELECT evento.id_Evento, prezzo, evento.citta, breve_descrizione, data, evento.nome, immagine, nome_Tipologia_Evento, creatore FROM evento, tipologia_evento, carrello, user WHERE evento.id_Evento=carrello.id_Evento AND evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento AND carrello.username = user.username AND user.username=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            return null;
        }
        else{
            return $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public function isNotAccepted($username){
        $query = "SELECT accettazione FROM user WHERE username= ? AND id_TipologiaUser=3 AND accettazione='f'";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function eventsMoreMen(){
        $query = "SELECT evento.id_Evento, nome, breve_descrizione, tipologia_evento.nome_Tipologia_Evento as categoria, data, citta, immagine, luogo FROM evento, tipologia_evento WHERE evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento AND numero_partecipanti_uomini > numero_partecipanti_donne";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function eventsMoreWomen(){
        $query = "SELECT evento.id_Evento, nome, breve_descrizione, tipologia_evento.nome_Tipologia_Evento as categoria, data, citta, immagine, luogo FROM evento, tipologia_evento WHERE evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento AND numero_partecipanti_donne > numero_partecipanti_uomini";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //tutte gli eventi creati da un creatore
    public function getEventByOrganizer($username){
        $query = "SELECT id_Evento,prezzo, evento.citta, descrizione, breve_descrizione, data,numero_Partecipanti_Max,evento.nome, luogo, indirizzo, numero_civico, numero_partecipanti_uomini, numero_partecipanti_donne, immagine,tipologia_evento.nome_Tipologia_Evento as categoria FROM user,evento, tipologia_evento WHERE username= ? AND user.username=evento.creatore AND evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCities(){
        $stmt = $this->db->prepare("SELECT DISTINCT citta FROM EVENTO");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteEventInCart($id, $username){
        $stmt = $this->db->prepare("DELETE FROM carrello WHERE id_Evento = ? AND username = ?");
        $stmt->bind_param('is',$id, $username);
        return $stmt->execute();
    }

    public function deleteAllEventInCartOfUser($username){
        $stmt = $this->db->prepare("DELETE FROM carrello WHERE username = ?");
        $stmt->bind_param('s', $username);
        return $stmt->execute();
    }

    public function addCategory($category){
        $query = "INSERT INTO TIPOLOGIA_EVENTO(nome_Tipologia_Evento) VALUES (?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$category);
        return $stmt->execute();
    }

    public function getAllTypeOfEvent(){
       $stmt = $this->db->prepare("SELECT * FROM tipologia_evento ");
       $stmt->execute();
       $result = $stmt->get_result();
       return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllUsers(){
       $stmt = $this->db->prepare("SELECT nome, cognome, username FROM user WHERE id_TipologiaUser=2");
       $stmt->execute();
       $result = $stmt->get_result();
       return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllOrganizers(){
        $stmt = $this->db->prepare("SELECT nome, cognome, username FROM user WHERE id_TipologiaUser=3");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllNotAcceptedOrganizers(){
        $stmt = $this->db->prepare("SELECT nome, cognome, username FROM user WHERE id_TipologiaUser=3 AND accettazione = 'f'");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteUser($username){
        $stmt = $this->db->prepare("DELETE FROM `user` WHERE username = ?");
        $stmt->bind_param('s',$username);
        $stmt->execute();
        return $stmt->execute();
    }

    public function isPresentEventInCartOfUsers($id, $username){
        $stmt = $this->db->prepare("SELECT * FROM carrello WHERE id_Evento = ? AND username = ?");
        $stmt->bind_param('is', $id, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function addEventInCartOfUsers($id, $username){
        $stmt = $this->db->prepare("INSERT INTO `carrello`(`id_Evento`, `username`) VALUES (?,?)");
        $stmt->bind_param('is', $id, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    public function getNotifyForUser($username){
        $stmt = $this->db->prepare("SELECT tipologia_notifica.descrizione, nome, immagine, evento.data, evento.prezzo, evento.id_Evento, tipologia_evento.nome_Tipologia_Evento FROM NOTIFICA, TIPOLOGIA_NOTIFICA, EVENTO, ACQUISTI, tipologia_evento WHERE NOTIFICA.id_Tipologia_Notifiche=TIPOLOGIA_NOTIFICA.id_Tipologia_Notifiche AND NOTIFICA.id_Evento=EVENTO.id_Evento AND id_TipologiaUser=(SELECT id_TipologiaUser FROM USER WHERE username=?) AND ACQUISTI.id_Evento=NOTIFICA.id_Evento AND tipologia_evento.id_Tipologia_Evento=evento.id_Tipologia_Evento AND ACQUISTI.username=?");
        $stmt->bind_param('ss', $username, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            return null;
        }
        else{
            return $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public function addPurchase($id, $username, $data){
        $stmt = $this->db->prepare("INSERT INTO `acquisti`(`id_Evento`, `username`, `data`) VALUES (?,?,?)");
        $stmt->bind_param('iss', $id, $username, $data);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    public function checkUserLogin($username, $password) {
        if ($stmt = $this->db->prepare("SELECT username, pass, salt FROM USER WHERE username = ? AND accettazione = 't' AND (id_TipologiaUser=2 OR id_TipologiaUser=3) LIMIT 1")) { 
           $stmt->bind_param('s', $username); 
           $stmt->execute(); // esegue la query appena creata.
           $stmt->store_result();
           $stmt->bind_result($username, $db_password, $salt); // recupera il risultato della query e lo memorizza nelle relative variabili.
           $stmt->fetch();
           $password = hash('sha512', $password.$salt); // codifica la password usando una chiave univoca.
           if($stmt->num_rows == 1) { // se l'utente esiste
              if($db_password == $password) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
                 // Password corretta!          
                    $_SESSION['username'] = $username;
                    // Login eseguito con successo.
                    return 1;    
              } else {
                  return -1;
              }
           } else {
              // L'utente inserito non esiste.
              return -1;
           }
        }
    }

    public function checkAdminLogin($username, $password){
        if ($stmt = $this->db->prepare("SELECT username, pass, salt FROM USER WHERE username = ? AND accettazione = 't' AND id_TipologiaUser=1 LIMIT 1")) { 
            $stmt->bind_param('s', $username); 
            $stmt->execute(); // esegue la query appena creata.
            $stmt->store_result();
            $stmt->bind_result($username, $db_password, $salt); // recupera il risultato della query e lo memorizza nelle relative variabili.
            $stmt->fetch();
            $password = hash('sha512', $password.$salt); // codifica la password usando una chiave univoca.
            if($stmt->num_rows == 1) { // se l'utente esiste
               if($db_password == $password) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
                  // Password corretta!            
                     $_SESSION['admin'] = $username;
                     // Login eseguito con successo.
                     return 1;    
               } else {
                return -1;
            }
            } else {
               // L'utente inserito non esiste.
               return -1;
            }
         }
    } 

    public function getTipologiaEvento($nome_Tipologia_Evento){
        $stmt=$this->db->prepare("SELECT id_Tipologia_Evento FROM tipologia_evento WHERE nome_Tipologia_Evento=? LIMIT 1");
        $stmt->bind_param('s',$nome_Tipologia_Evento);

        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getIdEventOfUserInCart($username){
        $stmt=$this->db->prepare("SELECT id_Evento FROM carrello WHERE username = ?");
        $stmt->bind_param("s",$username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function addNotify($id_TipologiaUser,$id_Tipologia_Notifica,$id_Tipologia_Evento){
        $query = "INSERT INTO notifica (id_TipologiaUser, id_Tipologia_Notifiche, id_Evento) VALUES (?,?,?)";
        $stmt = $this->db->prepare($query);        
        $stmt->bind_param("iii",$id_TipologiaUser,$id_Tipologia_Notifica,$id_Tipologia_Evento);
        $stmt->execute();
        return $stmt->get_result();
    }
    public function maxIdEvent(){
        $stmt = $this->db->prepare("SELECT MAX(id_Evento) as maxId from evento");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function addParteciperOnEvent($id, $username){
        $stmt=$this->db->prepare("SELECT * FROM user WHERE username = ? AND sesso = ?");
        $sesso = "F";
        $stmt->bind_param("ss", $username, $sesso);
        $stmt->execute();
        $result = $stmt->get_result();
        $result->fetch_all(MYSQLI_ASSOC);
        if(mysqli_num_rows($result)==0){
            $stmt=$this->db->prepare("UPDATE evento SET numero_partecipanti_uomini = numero_partecipanti_uomini + 1 WHERE id_Evento = ?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
        }
        else{
            $stmt=$this->db->prepare("UPDATE evento SET numero_partecipanti_donne = numero_partecipanti_donne + 1 WHERE id_Evento = ?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
        }
    }

    public function getNotifyForOrganizer($username){
        $stmt = $this->db->prepare("SELECT tipologia_notifica.descrizione, nome, immagine, evento.data, evento.prezzo, evento.id_Evento, tipologia_evento.nome_Tipologia_Evento FROM NOTIFICA, TIPOLOGIA_NOTIFICA, EVENTO, tipologia_evento WHERE NOTIFICA.id_Tipologia_Notifiche=TIPOLOGIA_NOTIFICA.id_Tipologia_Notifiche AND NOTIFICA.id_Evento=EVENTO.id_Evento AND id_TipologiaUser=(SELECT id_TipologiaUser FROM USER WHERE username=?) AND tipologia_evento.id_Tipologia_Evento=evento.id_Tipologia_Evento AND EVENTO.creatore=?");
        $stmt->bind_param('ss', $username, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            return null;
        }
        else{
            return $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public function isUser($username){
        $stmt = $this->db->prepare("SELECT * FROM user WHERE id_TipologiaUser = ? AND username = ?");
        $value = 2;
        $stmt->bind_param('is', $value, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function isOrganizer($username){
        $stmt = $this->db->prepare("SELECT * FROM user WHERE id_TipologiaUser = ? AND username = ?");
        $value = 3;
        $stmt->bind_param('is', $value, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function getPurchaseOfUser($username){
        $stmt = $this->db->prepare("SELECT evento.id_Evento, nome, breve_descrizione, tipologia_evento.nome_Tipologia_Evento, evento.data, citta, immagine, luogo FROM evento, tipologia_evento, acquisti WHERE evento.id_Tipologia_Evento = tipologia_evento.id_Tipologia_Evento AND evento.id_Evento = acquisti.id_Evento AND acquisti.username = ?");
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            return null;
        }
        else{
            return $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public function sumOfCartOfUserEvent($username){
        $query = "SELECT SUM(prezzo) as somme FROM evento, carrello WHERE evento.id_Evento=carrello.id_Evento AND carrello.username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function soldOutEvent($id){
        $stmt = $this->db->prepare("SELECT * FROM notifica WHERE id_Tipologia_Notifiche = 4 AND id_Evento = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            $stmt = $this->db->prepare("INSERT INTO `notifica`(`id_TipologiaUser`, `id_Tipologia_Notifiche`, `id_Evento`) VALUES (3,4,?)");
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $result = $stmt->get_result();
            return $result;
        }
    }

    public function isSoldOutEvent($id){
        $stmt = $this->db->prepare("SELECT id_Evento, numero_partecipanti_Max, numero_partecipanti_uomini, numero_partecipanti_donne FROM evento WHERE id_Evento=?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $a = $result->fetch_all(MYSQLI_ASSOC);
        $b = $a[0];
        $sum = $b["numero_partecipanti_uomini"] + $b["numero_partecipanti_donne"];
        if($sum >= $b["numero_partecipanti_Max"]){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function isPresentEventInUserShop($id, $username){
        $stmt = $this->db->prepare("SELECT * FROM acquisti WHERE id_Evento = ? AND username = ?");
        $stmt->bind_param('is', $id, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        if (mysqli_num_rows($result)==0){
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function removePurchase($id, $username){
        $stmt = $this->db->prepare("DELETE FROM `acquisti` WHERE id_Evento = ? AND username = ?");
        $stmt->bind_param('is', $id, $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result;
    }

    public function removeParteciperOnEvent($id, $username){
        $stmt=$this->db->prepare("SELECT * FROM user WHERE username = ? AND sesso = ?");
        $sesso = "F";
        $stmt->bind_param("ss", $username, $sesso);
        $stmt->execute();
        $result = $stmt->get_result();
        $result->fetch_all(MYSQLI_ASSOC);
        if(mysqli_num_rows($result)==0){
            $stmt=$this->db->prepare("UPDATE evento SET numero_partecipanti_uomini = numero_partecipanti_uomini - 1 WHERE id_Evento = ?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
        }
        else{
            $stmt=$this->db->prepare("UPDATE evento SET numero_partecipanti_donne = numero_partecipanti_donne - 1 WHERE id_Evento = ?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
        }
    }
}
?>
