<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

if(isset($_GET["add"])){
    $dbh->addEventInCartOfUsers($_GET["add"], $_SESSION["username"]);
}

$evento = $dbh->getEventById($_GET["evento"]);
$templateParams["titolo"] = "PartYamo - ".$evento[0]["nome"];
$templateParams["evento"] = $evento[0];
$templateParams["nome"] = "evento-totale.php";
$templateParams["home"] = true;
if(isset($_GET["az"])){
    if($_GET["az"]==2){
        $templateParams["notifica"] = 2;
    }

    if($_GET["az"]==3){
        $templateParams["notifica"] = 3;
    }
}
else{
    $templateParams["notifica"] = 1;
}

if($dbh->isUser($_SESSION["username"])){
    $templateParams["nav"] = true;
    $templateParams["js"] = array('js/navSlide.js');
    $templateParams["user"] = true;
}
else{
    $templateParams["navOrganize"]=true;
}


require 'template/base.php';
?>