<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

$templateParams["titolo"] = "PartYamo - Acquisti";
$templateParams["nome"] = "eventi-carrello.php";
$templateParams["eventi"] = $dbh->getPurchaseOfUser($_SESSION["username"]);
$templateParams["mode"] = "eventi-acquistati";
$templateParams["nav"] = true;
$templateParams["home"] = true;
$templateParams["js"] = array('js/navSlide.js');

require 'template/base.php';
?>