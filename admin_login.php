<?php
require_once 'bootstrap.php';
session_unset();

if(isset($_POST["inputUsername"]) && isset($_POST["inputPassword"])){
    $login_result = $dbh->checkAdminLogin($_POST["inputUsername"], $_POST["inputPassword"]);
    if($login_result){
        //Login fallito
        $templateParams["errorelogin"] = "Errore! Controllare username o password!";
    }
}


if(isAdminLoggedIn()){
    header("Location: admin.php");
    exit;
}
else{
    $templateParams["titolo"] = "PartYamo - Login";
    $templateParams["descrizione"] = "Area Riservata";
    $templateParams["nome"] = "login-form.php";
    $templateParams["js"] = array('js/validation.js');
    $templateParams["admin"] = true;
}

    require 'template/base.php';
?>