<?php
require_once 'bootstrap.php';
session_unset();

if(isset($_POST["inputUsername"]) && isset($_POST["inputPassword"])){
    $login_result = $dbh->checkUserLogin($_POST["inputUsername"], $_POST["inputPassword"]);
    if($login_result < 0){
        //Login fallito
        $templateParams["errorelogin"] = "Errore! Controllare username o password!";

        $login_result = $dbh->isNotAccepted($_POST["inputUsername"]);
        if(count($login_result) > 0){
            $templateParams["errorelogin"] = "Attenzione! La tua richiesta di registrazione è ancora in fase di accettazione";
        }
    }
}

if(isUserLoggedIn()){
    if($dbh->isUser($_SESSION["username"])){
        header("Location: user_home.php");
        exit;
    }
    else{
        header("Location: organizer_home.php");
        exit;
    }
}
else{
    $templateParams["titolo"] = "PartYamo - Login";
    $templateParams["descrizione"] = "Accedi";
    $templateParams["user"] = true;
    $templateParams["nome"] = "login-form.php";
    $templateParams["js"] = array('js/validation.js');
}

    require 'template/base.php';
?>