<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

if(isset($_GET["action"])){
    if($_GET["action"] == 1){
        $eventi = $dbh->getIdEventOfUserInCart($_SESSION["username"]);
        $dbh->deleteAllEventInCartOfUser($_SESSION["username"]);
        foreach($eventi as $evento){
            $dbh->addPurchase($evento["id_Evento"], $_SESSION["username"], date("Y/m/d"));
            $dbh->addParteciperOnEvent($evento["id_Evento"], $_SESSION["username"]);
            $h = $dbh->getEventById($evento["id_Evento"]);
            $currentEvent = $h[0];
            $sum = $currentEvent["numero_partecipanti_uomini"] + $currentEvent["numero_partecipanti_donne"];
            if($sum >= $currentEvent["numero_partecipanti_Max"]){
                $dbh->soldOutEvent($evento["id_Evento"]);
            }
        }
        $templateParams["nome"] = "acquisto-avvenuto.php";
    }

    if($_GET["action"] == 2){
        $dbh->addPurchase($_GET["evento"], $_SESSION["username"], date("Y/m/d"));
        $dbh->addParteciperOnEvent($_GET["evento"], $_SESSION["username"]);
        $h = $dbh->getEventById($_GET["evento"]);
        $currentEvent = $h[0];
        $sum = $currentEvent["numero_partecipanti_uomini"] + $currentEvent["numero_partecipanti_donne"];
        if($sum >= $currentEvent["numero_partecipanti_Max"]){
            $dbh->soldOutEvent($_GET["evento"]);
        }
        
        header("location: evento.php?evento=".$_GET["evento"]."&az=2");
    }

    if($_GET["action"] == 3){
        $dbh->removePurchase($_GET["evento"], $_SESSION["username"]);
        $dbh->removeParteciperOnEvent($_GET["evento"], $_SESSION["username"]);
        header("location: evento.php?evento=".$_GET["evento"]."&az=3");
    }

}else{
    $templateParams["nome"] = "eventi-acquista.php";
}


$templateParams["titolo"] = "PartYamo - Acquisto";
$templateParams["nav"] = true;
$templateParams["home"] = true;
$templateParams["js"] = array('js/navSlide.js');

require 'template/base.php';
?>