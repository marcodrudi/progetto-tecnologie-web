<?php
    require_once 'bootstrap.php';
    session_unset();

    //Base Template
    $templateParams["titolo"] = "PartYamo";
    $templateParams["evento"] = $dbh->getRandomEvents();
    $templateParams["nome"] = 'title.php';
    
    require 'template/base.php';
?>