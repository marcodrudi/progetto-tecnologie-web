<?php
    require_once 'bootstrap.php';

    //controlla se  è inserito
    if(!isUserLoggedIn()){
        header("location: login.php");
    }
    
    if(isset($_GET["action"])){
        
        if($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3){
            header("location: login.php");
        }

        if($_GET["action"]!=1){//voglio o modificare o eliminare
            
            $risultato = $dbh->getEventById($_GET["id"]);
            
            
            if(!isset($risultato)){
                $templateParams["evento"] = null;
            }
            else{
                $templateParams["evento"] = $risultato[0];
                
                //modifico
                if($_GET["action"]==2){
                    if(isset($_FILES["immagine1"]) && strlen($_FILES["immagine1"]["name"])>0){
                        list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["immagine1"]);
                        if($result != 0){
                            $imgarticolo = $msg;
                        }else{
                            $imgarticolo = $_POST["oldimg"];
                        }
                    }
                    else{
                        $imgarticolo = $_POST["oldimg"];
                    }
                    $id_Tipologia_Evento=$dbh->getTipologiaEvento($_POST["tipologia_eventi"])[0]["id_Tipologia_Evento"];  

                    $dbh->updateEvent($templateParams["evento"]["id_Evento"],$_POST["prezzo"],$_POST["citta"], $_POST["descrizione"], $_POST["breve_descrizione"], $_POST["data"], $_POST["numero_partecipanti_Max"],$_POST["nome"], $_POST["luogo"], $_POST["indirizzo"], $_POST["numero_civico"],$imgarticolo,$_POST["orario"],$id_Tipologia_Evento);

                    
                }
                //elimino
                else{
                    $dbh->deleteEventById($templateParams["evento"]["id_Evento"]);
                }
            }
        }
        else{
            list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);
            if($result != 0){
                $imgarticolo = $msg;
                $id_Tipologia_Evento=$dbh->getTipologiaEvento($_POST["tipologia_eventi"])[0]["id_Tipologia_Evento"];
                $dbh->insertEvent($_POST["prezzo"],$_POST["citta"], $_POST["descrizione"], $_POST["breve_descrizione"], $_POST["data"], $_POST["numero_partecipanti_Max"],$_POST["nome"], $_POST["luogo"], $_POST["indirizzo"], $_POST["numero_civico"],0,0,$imgarticolo,$_POST["orario"],$id_Tipologia_Evento, $_SESSION["username"]);
                //creo la notifica relativa alla corretta crezione
                $maxid=$dbh->maxIdEvent()[0]["maxId"];
                $dbh->addNotify(3,3,$maxid);
            }
        }

    }
    
    //Base Template
    $templateParams["titolo"] = "PartYamo - Organizer Home";
    $templateParams["navOrganize"]=true;
    $templateParams["notOrg"] = true;
    $templateParams["nome"]="organizer_events.php";
    $templateParams["tipologia_eventi"]=$dbh->getAllTypeOfEvent();
    $templateParams["evento"]=$dbh->getEventByOrganizer($_SESSION["username"]);
    
    
    

    
    require 'template/base.php';


    
    
    

    
?>