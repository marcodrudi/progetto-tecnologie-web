<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

$templateParams["titolo"] = "PartYamo - Notifiche";

if($dbh->isUser($_SESSION["username"])){
    $templateParams["notifiche"] = $dbh->getNotifyForUser($_SESSION["username"]);
    $templateParams["nav"] = true;
    $templateParams["js"] = array('js/navSlide.js');
}

if($dbh->isOrganizer($_SESSION["username"])){
    $templateParams["notifiche"] = $dbh->getNotifyForOrganizer($_SESSION["username"]);
    $templateParams["navOrganize"]=true;
}

$templateParams["nome"] = "notifica.php";
$templateParams["home"] = true;

require 'template/base.php';
?>