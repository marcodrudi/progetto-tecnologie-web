<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

//in base ad action faccio le mie cose
if(isset($_GET["id"])){
    $templateParams["evento"] = $dbh->getEventById($_GET["id"])[0];
}
else{
    $templateParams["evento"] =getEmptyEvents();
}

$templateParams["titolo"] = "PartYamo - Gestione Evento";
$templateParams["navOrganize"]=true;
$templateParams["nome"]="modify_events.php";
$templateParams["js"] = array('js/validation.js');

require 'template/base.php'

?>